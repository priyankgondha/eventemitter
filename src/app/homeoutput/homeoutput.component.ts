import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-homeoutput',
  templateUrl: './homeoutput.component.html',
  styleUrls: ['./homeoutput.component.css']
})
export class HomeoutputComponent implements OnInit {

  @Output() newItemEvent = new EventEmitter<string>();
  @Output() public sendData = new EventEmitter<string>();

  valid:boolean=false;

  // if(!=null){

  // }
  addNewItem(value: string) {
    this.newItemEvent.emit(value);
  }

  constructor() { }

  ngOnInit(): void {
  }

}
