import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeoutputComponent } from './homeoutput.component';

describe('HomeoutputComponent', () => {
  let component: HomeoutputComponent;
  let fixture: ComponentFixture<HomeoutputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeoutputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeoutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
