import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';
import { of } from 'rxjs';
// import EventEmitter = require('events');

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


   @Input() parentDataName :string;
  @Input() parentDatasurname:string;
  @Output() newItemEvent = new EventEmitter<string>();
  @Output() public sendData = new EventEmitter<string>();

  valid:boolean=true;

  // if(!=null){

  // }
  addNewItem(value: string) {
    this.newItemEvent.emit(value);
  }
  ngOnInit(): void {
    this.sendData.emit('ChildData');
  }

}
