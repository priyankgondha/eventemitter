import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'demo3';
  enterName = '';
  parentDataName = "";
  parentDatasurname= '';
  enterSurname = "";
  value;
  valid:boolean=false;

  transfer(){
    this.parentDataName = this.enterName;
    this.parentDatasurname = this.enterSurname;
    this.valid == true;
  }

  show(){
    this.valid==true;
  }
  sendData(value){
    this.value = value;
  }

  items = ['item1', 'item2', 'item3', 'item4'];
  count =0;
  addItem(newItem: string) {
    this.items.push(newItem);
  }
}
